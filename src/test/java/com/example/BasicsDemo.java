package com.example;

import com.example.classprinter.Sample;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static com.example.util.ReflectionUtils.getModifiers;
import static com.example.util.ReflectionUtils.getParameters;
import static com.example.util.ReflectionUtils.getType;

public class BasicsDemo {

    @BeforeEach
    void setUp() {
        System.out.println();
        System.out.println("----------------------------------------\n");
    }

    @AfterEach
    void after() {
        System.out.println("\n----------------------------------------");
        System.out.println();
    }


    @Test
    void demoGetClass() {
        Class clazz = Sample.class;

        // print package name
        System.out.println("class: " + clazz.getSimpleName() + " ");
        System.out.println("class: " + clazz.getName() + " ");
        System.out.println("package: " + clazz.getPackage().getName() + ";");
    }

    @Test
    void demoGetSuperClass() {
        Class clazz = Object.class;

        // print package name
        System.out.println("extends: " + clazz.getSuperclass() + " ");
    }

    @Test
    void demoGetInterfaces() {
        Class clazz = Sample.class;

        // print implemented interfaces of this class
        Class[] interfaces = clazz.getInterfaces();
        for (int i = 0, size = interfaces.length; i < size; i++) {
            System.out.print(i == 0 ? "implements: " : ", ");
            System.out.print(interfaces[i].getSimpleName());
        }
    }

    @Test
    void demoGetModifiers() throws NoSuchMethodException {
        Class clazz = Sample.class;

        int modifiers = clazz.getModifiers();
        Method ownPrivateMethod = clazz.getDeclaredMethod("ownPrivateMethod");
        System.out.println(ownPrivateMethod.isAccessible());
        ownPrivateMethod.setAccessible(true);
        System.out.println(ownPrivateMethod.isAccessible());
    }


    @Test
    void demoGetFields() {
        Class clazz = Sample.class;

        // print fields of this class
        Field[] fields = clazz.getFields();
        for (Field field : fields) {
            System.out.println(getModifiers(field.getModifiers()) + getType(field.getType()) + " " + field.getName());
        }
    }

    @Test
    void demoGetConstructors() {
        Class clazz = Sample.class;

        // print class's constructors
        Constructor[] constructors = clazz.getConstructors();
        for (Constructor c : constructors) {
            System.out.println();
            System.out.print(getModifiers(c.getModifiers()) + clazz.getSimpleName() + "(");
            System.out.print(getParameters(c.getParameterTypes()));
            System.out.println(")");
        }
    }

    @Test
    void demoGetMethods() {
        Class clazz = Sample.class;

        System.out.println(clazz.isInterface());

        // print class's methods
        Method[] methods = clazz.getMethods();
        for (Method m : methods) {
            printAnnotations(m);

            System.out.print(getModifiers(m.getModifiers()) + getType(m.getReturnType()) + " " + m.getName() + "(");
            System.out.print(getParameters(m.getParameterTypes()));
            System.out.println(")");
        }
    }

    private void printAnnotations(Method m) {
        // retrieve annotations
        Annotation[] annotations = m.getAnnotations();
        System.out.println();
        for (Annotation a : annotations) {
            System.out.print("@" + a.annotationType().getSimpleName() + " ");
            System.out.println();
        }
    }

    @Test
    void demoCreateInstance() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchFieldException, ClassNotFoundException {
        Class<Sample> clazz = Sample.class;
        Constructor<Sample> cons = clazz.getConstructor(Integer.class);

        Sample bird2 = cons.newInstance(Integer.valueOf("5"));

        System.out.println(bird2.getIntField());

    }
}
