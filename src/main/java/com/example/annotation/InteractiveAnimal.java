package com.example.annotation;

public interface InteractiveAnimal {

    @AnimalTranslator()
    String sayingSmth(String meow);

    String letDownWaterGlass(Boolean justDoIt);
}
