package com.example.logging;

import java.util.Objects;

public class DemoLogging2 {
    private int val1 = 1;
    private int val2 = 2;

    public static void main(String[] args) throws NoSuchMethodException {
        LoggingDemo demo = new LoggingDemo(new DemoLogging2());
        System.out.println(demo.calculate());
        System.out.println("====");
        System.out.println(demo.getVal1());
    }

    @Logged
    private int calculate() {
        return this.val1 + this.val2;
    }

    public int getVal1() {
        return val1;
    }

    private static class LoggingDemo {
        private DemoLogging2 obj;

        public LoggingDemo(DemoLogging2 obj) {
            this.obj = obj;
        }

        public int calculate() throws NoSuchMethodException {
            logIfAnnotated("calculate", "Start calculate()");
            return obj.calculate();
        }

        public int getVal1() throws NoSuchMethodException {
            logIfAnnotated("getVal1", "Get value 1");
            return obj.getVal1();
        }

        private void logIfAnnotated(String methodName, String text) throws NoSuchMethodException {
            Logged annotation = obj.getClass()
                    .getDeclaredMethod(methodName)
                    .getAnnotation(Logged.class);
            if (Objects.nonNull(annotation)) {
                System.out.println(text);
            }
        }

    }
}
