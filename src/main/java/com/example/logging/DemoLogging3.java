package com.example.logging;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class DemoLogging3 implements LoggedType {
    private int val1 = 1;
    private int val2 = 2;

    public static void main(String[] args) {
        LoggedType loggedProxy = (LoggedType) Proxy.newProxyInstance(
                DemoLogging3.class.getClassLoader(),
                DemoLogging3.class.getInterfaces(),
                new LoggingDemo(new DemoLogging3()));

        System.out.println(loggedProxy.calculate());
        System.out.println("====");
        System.out.println(loggedProxy.getVal1());
    }

    public int calculate() {
        return this.val1 + this.val2;
    }

    public int getVal1() {
        return val1;
    }

    private static class LoggingDemo implements InvocationHandler {
        private DemoLogging3 obj;

        public LoggingDemo(DemoLogging3 obj) {
            this.obj = obj;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            if (method.getName().equals("calculate")) {
                System.out.println("Start " + method.getName() + "()");
            }
            if (method.getName().equals("getVal1")) {
                System.out.println("Get val 1");
            }
            return method.invoke(obj, args);
        }

    }
}
