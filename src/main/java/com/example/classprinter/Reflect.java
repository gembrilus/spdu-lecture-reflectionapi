package com.example.classprinter;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static com.example.util.ReflectionUtils.getModifiers;
import static com.example.util.ReflectionUtils.getParameters;
import static com.example.util.ReflectionUtils.getType;

/**
 * Source of example: http://www.javenue.info/post/84
 */

class Reflect {
    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Class clazz = Sample.class;

        System.out.println();
        System.out.println();

        pringPackage(clazz);

        printModifiers(clazz);
        printSimpleName(clazz);
        printParentClassName(clazz);

        printInterfaces(clazz);
        System.out.println(" {");

        printFields(clazz);
        printConstructors(clazz);
        printMethods(clazz);

        System.out.println("}");

        System.out.println();
        System.out.println();
    }

    private static void printMethods(Class clazz) {
        // print class's methods
        Method[] methods = clazz.getDeclaredMethods();
        for (Method m : methods) {
            // retrieve annotations
            Annotation[] annotations = m.getAnnotations();
            System.out.print("\t");
            for (Annotation a : annotations) {
                System.out.println();
                System.out.print("\t");
                System.out.print("@" + a.annotationType().getSimpleName() + " ");
            }
            System.out.println();

            System.out.print("\t" + getModifiers(m.getModifiers()) + getType(m.getReturnType()) + " " + m.getName() + "(");
            System.out.print(getParameters(m.getParameterTypes()));
            System.out.println(") { }");
        }
    }

    private static void printConstructors(Class clazz) {
        // print class's constructors
        Constructor[] constructors = clazz.getDeclaredConstructors();
        for (Constructor c : constructors) {
            System.out.println();
            System.out.print("\t" + getModifiers(c.getModifiers()) + clazz.getSimpleName() + "(");
            System.out.print(getParameters(c.getParameterTypes()));
            System.out.println(") { }");
        }
    }

    private static void printFields(Class clazz) {
        // print fields of this class
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            System.out.println("\t" + getModifiers(field.getModifiers()) + getType(field.getType()) + " " + field.getName() + ";");
        }
    }

    private static void printInterfaces(Class clazz) {
        // print implemented interfaces of this class
        Class[] interfaces = clazz.getInterfaces();
        for (int i = 0, size = interfaces.length; i < size; i++) {
            System.out.print(i == 0 ? "implements " : ", ");
            System.out.print(interfaces[i].getSimpleName());
        }
    }

    private static void printParentClassName(Class clazz) {
        // print parent class's name
        System.out.print("extends " + clazz.getSuperclass().getSimpleName() + " ");
    }

    private static void printSimpleName(Class clazz) {
        // print class name
        System.out.print("class " + clazz.getSimpleName() + " ");
    }

    private static void printModifiers(Class clazz) {
        // start class declaration from it's methods
        int modifiers = clazz.getModifiers();
        System.out.print(getModifiers(modifiers));
    }

    private static void pringPackage(Class clazz) {
        // print package name
        Package p = clazz.getPackage();
        System.out.println("package " + p.getName() + ";");
    }
}
